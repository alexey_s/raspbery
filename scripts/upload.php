<?php
if ( !empty( $_FILES ) ) {


    $tempPath = $_FILES[ 'Files' ][ 'tmp_name' ];
    $uploadPath = "../uploads/".$_FILES[ 'Files' ][ 'name' ];
    move_uploaded_file( $tempPath, $uploadPath );
    $answer = array( 'answer' => 'File transfer completed' );
    $json = json_encode( $answer );
    echo $json;
} else {
    echo 'No files';
}
?>