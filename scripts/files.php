<?php
/**
 * Created by PhpStorm.
 * User: ASokolov
 * Date: 29.10.2015
 * Time: 13:04
 */


$keys = ["deleteFile","scanDirectory","logDirectory"];

const imgpath = "/uploads/";


function logDirectory($logPath){
    $log = file_get_contents($logPath);
    echo $log;
}

function scanDirectory(){
    $res = array();
    //$res = glob($_POST["scanDirectory"].imgpath."*.{bmp,BMP}", GLOB_BRACE);
    $res = glob($_POST["scanDirectory"]."*.{bmp,BMP,jpg,JPG}", GLOB_BRACE);
    $files = array();
    foreach ($res as $file)
    {
        $files[]= ['fileName'=>basename($file)];
    }

    echo(json_encode($files));
    exit;
}

function deleteFile(){
    $answer = array( 'answer' => 'undefined' );

    if(file_exists($_POST["dir"].$_POST["deleteFile"])){
            if(unlink($_POST["dir"].$_POST["deleteFile"])){
                $answer = array( 'answer' => 'File deleted' );
            } else {
                $answer = array( 'answer' => 'Can`t delete file' );
            }
    }else{
        $answer = array( 'answer' => 'No such file!!!' );
    }
    $json = json_encode($answer);
    echo $json;

}



foreach ($keys as $key){

    if (isset($_POST[$key])){
            $key($_POST[$key]);
    break;
    }

}

?>