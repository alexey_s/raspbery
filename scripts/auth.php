<?php
session_start(); //Запускаем сессии


class AuthClass {

    /**
     * Проверяет, авторизован пользователь или нет
     * Возвращает true если авторизован, иначе false
     * @return boolean
     */
    public function isAuth() {
        if (isset($_SESSION["is_auth"])) { //Если сессия существует
            return $_SESSION["is_auth"]; //Возвращаем значение переменной сессии is_auth (хранит true если авторизован, false если не авторизован)
        }
        else return false; //Пользователь не авторизован, т.к. переменная is_auth не создана
    }

    /**
     * Авторизация пользователя
     * @param string $login
     * @param string $passwors
     */
    public function auth($login, $passwors) {
        $data = json_decode(file_get_contents('config/auth.db'), true);
        if ($login == $data['login'] && md5($passwors) == $data['password']) { //Если логин и пароль введены правильно
            $_SESSION["is_auth"] = true; //Делаем пользователя авторизованным
            $_SESSION["login"] = $login; //Записываем в сессию логин пользователя
            return true;
        }
        else { //Логин и пароль не подошел
            $_SESSION["is_auth"] = false;
            return false;
        }
    }

    /**
     * Смена пароля/логина
     * @param string $login
     * @param string $password
     * @return int
     */
    public function changeAuth($login, $password)
    {
        return file_put_contents('../config/auth.db', json_encode(['login' => $login, 'password' => md5($password)]));
    }

    /**
     * Метод возвращает логин авторизованного пользователя
     */
    public function getLogin() {
        if ($this->isAuth()) { //Если пользователь авторизован
            return $_SESSION["login"]; //Возвращаем логин, который записан в сессию
        }
    }


    public function out() {
        $_SESSION = array(); //Очищаем сессию
        session_destroy(); //Уничтожаем
    }
}

$auth = new AuthClass();


if (isset($_GET["ch_passwd"])) { //Если нажата кнопка выхода
    if (isset($_POST["login"]) && isset($_POST["password"])) { //Если логин и пароль были отправлены
        $auth->changeAuth($_POST["login"],$_POST["password"]);
        $auth->out(); //Выходим
        header("Location: /"); //Редирект после выхода
        exit;
    }
}




if (isset($_POST["login"]) && isset($_POST["password"])) { //Если логин и пароль были отправлены
    if (!$auth->auth($_POST["login"], $_POST["password"])) { //Если логин и пароль введен не правильно

        echo '<div class="alert alert-danger alert-dismissible" role="alert">
                        <a class="close" data-dismiss="alert" href="#">×</a>Ошибка авторизации!
        </div>';
    }
}

if (isset($_GET["is_exit"])) { //Если нажата кнопка выхода
    if ($_GET["is_exit"] == 1) {
        $auth->out(); //Выходим
        header("Location: ?is_exit=0"); //Редирект после выхода
    }
}



?>