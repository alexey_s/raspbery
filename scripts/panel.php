<?php
/**
 * Created by PhpStorm.
 * User: devaccess
 * Date: 09.10.2015
 * Time: 23:52
 */
?>
<div ng-app="myApp">

    <form name="myForm" ng-controller="FormController" ng-submit="submitMyForm()">
        <nav class="navbar navbar-default">
            <div class="container">
                <ul class="nav navbar-nav navbar-left">
                    <li id="messages" ng-show="message">{{ message }}</li>
                </ul>
                <ul class="nav navbar-nav navbar-right">

                    <li><a href="#">Здравствуйте, <?php echo $auth->getLogin(); ?></a></li>
                    <li><a href="#myModal" data-toggle="modal">Изменить пароль</a></li>
                    <li><a href="?is_exit=1">Выйти</a></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <div class="main__content">



                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Файлы</a></li>
                    <li><a data-toggle="tab" href="#menu1">Веб-камера</a></li>
                    <li><a data-toggle="tab" href="#menu2">Настройки</a></li>
                    <li><a data-toggle="tab" href="#menu3">Отладка</a></li>
                    <li class="navbar-right button__block">
                        <button type="button" class="main__submit_btn main__submit_btn_left btn btn-success " ng-click="send('0xff')">Старт</button>
                        <button type="button" class="main__submit_btn btn btn-danger" ng-click="send('0x00')">Стоп</button>
                    </li>
                </ul>

                <div class="tab-content">
                    <!--Вкладка ФАЙЛЫ-->
                    <div id="home" class="tab-pane fade in active">
                        <div class="main-tab__block">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="block__leftCol">

                                        <h4 class="block__subtitle">
                                            <label class="btn btn-default download__btn">
                                                <input type="file" class="file-input" multiple="multiple" oi-file="options">
                                                <i class="fa fa-upload"> Загрузить файл</i>
                                            </label>
                                        </h4>

                                        <ol id="file_list">
                                            <li data-ng-repeat="file in formData.files">
                                                <input type="checkbox"><a href="#" ng-click="show(file.fileName,'media')" data-id="0">{{file.fileName}}</a>
                                                <span ng-click="del(file.fileName,formData.media_path)"><i class="fa fa-ban"></i></span>
                                            </li>
                                        </ol>

                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="preview__block">
                                        <i class="fa fa-camera fa-5x"></i>
                                        <img src="{{preview}}">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--Вкладка ВЕБ-КАМЕРА-->
                    <div id="menu1" class="tab-pane fade">
                        <div class="main-tab__block">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="block__leftCol block__leftCol-mtop">
                                        <ol id="file_list">
                                            <li data-ng-repeat="file in formData.cam">
                                                <a href="#" ng-click="show(file.fileName,'cam')" data-id="0">{{file.fileName}}</a>
                                                <span ng-click="del(file.fileName,formData.cam_path)"><i class="fa fa-ban"></i></span>
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="preview__block">
                                        <i class="fa fa-camera fa-5x"></i>
                                        <img src="{{preview}}">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--Вкладка НАСТРОЙКИ-->
                    <div id="menu2" class="tab-pane fade">
                        <div class="main-tab__block">
                            <h3 class="block__title">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-floppy-o"> Настройки </i>
                                </button></h3>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input__block">
                                        <div class="row">
                                            <form class="form-horizontal">

                                                <div class="form-group">
                                                    <label for="input_4" class="col-xs-12 col-sm-3 control-label">Путь к изображениям:</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input name="media_path" ng-model="formData.media_path"  id="input_4" type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="input_9" class="col-xs-12 col-sm-3 control-label">Время показа (сек)</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input name="showtime" ng-model="formData.showtime" id="input_9" type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="input_5" class="col-xs-12 col-sm-3 control-label">Путь к снимкам веб-камеры:</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input name="cam_path" ng-model="formData.cam_path"  id="input_5" type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="input_2" class="col-xs-12 col-sm-3 control-label">Путь log файла:</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input name="log_path" ng-model="formData.log_path"  id="input_2" type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="input_3" class="col-xs-12 col-sm-3 control-label">Порт TCP:</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input name="tcp_port" ng-model="formData.tcp_port" id="input_3" type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="input_6" class="col-xs-12 col-sm-3 control-label">Четность:</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <!-- <input name="paryty" ng-model="formData.paryty" id="input_6" type="text" class="form-control"> -->
                                                        <select class="nya-selectpicker" ng-model="formData.paryty">
                                                            <option ng-repeat="parity in paritys" value="{{parity}}">{{parity}}</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="input_7" class="col-xs-12 col-sm-3 control-label">Адрес (CPU1):</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input name="adr1" ng-model="formData.adr1" id="input_7" type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="input_8" class="col-xs-12 col-sm-3 control-label">Адрес (CPU2):</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input name="adr2" ng-model="formData.adr2" id="input_8" type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="input_10" class="col-xs-12 col-sm-3 control-label">Задержка синхронизации (CPU1):</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input name="delay1" ng-model="formData.delay1" id="input_10" type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="input_11" class="col-xs-12 col-sm-3 control-label">Задержка синхронизации (CPU2):</label>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <input name="delay2" ng-model="formData.delay2" id="input_11" type="text" class="form-control">
                                                    </div>
                                                </div>


                                            </form>
                                        </div>
                                    </div>
                                 <!--   <div class="protocol__block">
                                        <h4 class="block__subtitle">Протокол:</h4>
                                        <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>Команда</th>
                                                <th>Запрос</th>
                                                <th>Ответ</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr ng-repeat="item in formData.original.elements" >
                                                <td><input ng-model="item.command"></td>
                                                <td><input ng-model="item.query"></td>
                                                <td><input ng-model="item.answer"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div> -->
                                    <div class="log__block">
                                        <h3 class="block__subtitle">
                                            <button class="btn btn-default" ng-click='loadLog()' type="button">
                                                <i class="fa fa-refresh"> Загрузить лог</i>
                                            </button>
                                        </h3>
                                        <textarea name="log__area" id="logs">{{logfile}}</textarea>
                                    </div>
                                    <div class="console__block">
                                        <h4 class="block__subtitle">Консоль:</h4>
                                        <textarea name="console__area" id="console" >{{console}}</textarea>

                                        <div class="input-group">
                                            <input type="text" class="form-control" ng-model="command" placeholder="Кодим здесь">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" ng-click="send(command)"><i class="fa fa-arrow-right"></i>
                                        </button>
                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Вкладка ОТЛАДКА-->
                    <div id="menu3" class="tab-pane fade">
                        <div class="main-tab__block">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Введите новый пароль</h4>
                </div>
                <div class="modal-body">
                    <form accept-charset="UTF-8" role="form" method="post" action="scripts/auth.php?ch_passwd=1">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Login" name="login" value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; // Заполняем поле по умолчанию ?>" type="text">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Сохранить">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>




</div> <!-- end angular -->

