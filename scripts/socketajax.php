<?php
require('socket.class.php');
require('socketClient.class.php');

if(!empty($_POST["command"]) && !empty($_POST["port"])){

    $socket = new socketClient('127.0.0.1', $_POST["port"]);
    $response = $socket->send($_POST["command"]);
    $data = array();
    $data['success'] = true;
    $data['answer'] = $response;
    echo json_encode($data);
   // echo $socket->report();
}

?>