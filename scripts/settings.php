<?php


class settings{

    private $path;

    public function __construct() {
        $this->path = $_SERVER["DOCUMENT_ROOT"]."/config/settings.ini";
    }

    public function set($dataJSON){
        file_put_contents($this->path,json_encode($dataJSON));
        $data = array();
        $data['success'] = true;
        $data['message'] = 'Данные сохранены успешно';
        return json_encode($data);
    }

    public function get(){
        if(file_exists($this->path)){
            $data = file_get_contents($this->path);
            return $data;
        } else {
            return false;
        }
    }
}


$settings = new settings();

if(isset($_POST["command"]) && $_POST["command"] == "getSettings"){
        echo $settings->get();
} elseif (isset($_POST["command"]) && $_POST["command"] == "setSettings"){
    if(isset($_POST["JSONdata"]) && (!empty($_POST["JSONdata"])) ){
        echo $settings->set($_POST["JSONdata"]);
    }
}