/**
 * Created by devaccess on 28.10.15.
 */
'use strict';
var mainApp = angular.module("myApp",
    [
        "ngResource",
        "oi.file",
        "ngSanitize",
        "nya.bootstrap.select",
        "com.2fdevs.videogular",
        "com.2fdevs.videogular.plugins.controls",
        "com.2fdevs.videogular.plugins.overlayplay",
        "com.2fdevs.videogular.plugins.poster",
        "com.2fdevs.videogular.plugins.buffering"
    ]);


mainApp.factory('Files', function ($resource) {
    return $resource('/scripts/upload.php/:fileId', {fileId:'@id'}, {
        add: {method: 'PUT'}
    });
});


mainApp.controller('FormController',['$scope','$http',"$sce","$timeout", 'Files', function ($scope, $http,$sce,$timeout, Files) {
    var controller = this;
    var empty = "";
    controller.state = null;
    controller.API = null;
    controller.currentVideo = 0;

    $scope.formData = {};
    $scope.answer = [];
    $scope.console =  "";
    $scope.list = "";
    $scope.formData.files = []; // Модель для файлов картинок
    $scope.formData.cam = []; // Модель для файлов веб-камеры
    $scope.preview = "../img/body.png";
    $scope.logfile = "";
    $scope.paritys = ['paryty', 'no_paryty'];



    $scope.loadLog = function(){
        var data = {};
        data.logDirectory = $scope.formData.log_path;
        data = $.param(data);

        $http({
            method: 'POST',
            url: '/scripts/files.php',
            data: data,  // pass in data as strings
            headers: {'Content-Type': 'application/x-www-form-urlencoded'} // set the headers so angular passing info as form data (not request payload)
        })


            .success(function (response) {
                $scope.logfile = response;
                console.log($scope.formData.files);
            });

    };



    $scope.scanDir = function (path,dest) {
        var data = {};
        data.scanDirectory = path;//$scope.formData.upload_path;
        data = $.param(data);

        $http({
            method: 'POST',
            url: '/scripts/files.php',
            data: data,  // pass in data as strings
            headers: {'Content-Type': 'application/x-www-form-urlencoded'} // set the headers so angular passing info as form data (not request payload)
        })


            .success(function (response) {
                if(dest == 'media') {
                    $scope.formData.files = response;
                }
                if(dest == 'cam'){
                    $scope.formData.cam =  response;
                }
            });
    };


    $scope.options = {
        //Вызывается для каждого выбранного файла
        change: function (file) {
            //В file содержится информация о файле
            //Загружаем на сервер


            file.$upload('/scripts/upload.php', $scope.formData.files, {allowedType: ["bmp", "BMP"]}).then(
                function (data) {
                    $scope.scanDir($scope.formData.media_path,'media');
                   // $scope.formData.files.push({'fileName':data.item.filename});
                  //  console.log(data.item.filename);
            });

        }
    };




    $scope.show = function(name,dest){
        if(dest == 'media') {
            $scope.preview = "/uploads/" + name;
        }
        if(dest == 'cam'){
            $scope.preview = "/cam/" + name;
        }

    };


    $scope.del = function (name,dir) {
        var data = {};
        data.deleteFile = name;
        data.dir = dir;
        data = $.param(data);

        $http({
            method: 'POST',
            url: '/scripts/files.php',
            data: data,  // pass in data as strings
            headers: {'Content-Type': 'application/x-www-form-urlencoded'} // set the headers so angular passing info as form data (not request payload)
        })
            .success(function(response) {
                $scope.scanDir($scope.formData.media_path,'media');
                $scope.scanDir($scope.formData.media_path,'cam');
                $scope.message = response.answer;
            });
    };



    $scope.getScope = function(){

        var data = {};
        data.command = "getSettings";
        data = $.param(data);

        $http({
            method: 'POST',
            url: '/scripts/settings.php',
            data: data,  // pass in data as strings
            headers: {'Content-Type': 'application/x-www-form-urlencoded'} // set the headers so angular passing info as form data (not request payload)
        })
            .success(function (data) {


                if (!data) {
                    // if not successful, bind errors to error variables
                    $scope.message = "Error";//data.errors.name;

                } else {

                     $scope.formData = data;


                    $scope.sources = [
                        {src: $sce.trustAsResourceUrl($scope.formData.video_path), type: "video/mp4"}
                    ];

                    $scope.scanDir($scope.formData.media_path,'media');
                    $scope.scanDir($scope.formData.cam_path,'cam');



                    return $scope.formData;


                }
            });

    };


    $scope.getScope();


    /*Save form data to settings.ini*/
    $scope.submitMyForm = function () {
        var data = {};
        data.JSONdata = $scope.formData;
        data.command = "setSettings";
        data = $.param(data);
        $http({
            method: 'POST',
            url: '/scripts/settings.php',
            data: data,  // pass in data as strings
            headers: {'Content-Type': 'application/x-www-form-urlencoded'} // set the headers so angular passing info as form data (not request payload)
        }).success(function(data) {
            // this callback will be called asynchronously
            // when the response is available
            $scope.message = data.message;


        }, function(data) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            $scope.message = "Error";//data.errors.name;
        });

    };


    $scope.send = function(command){

        var data = {};
        data.server = '127.0.0.1';
        data.port = $scope.formData.tcp_port;
        data.command = command;
        data = $.param(data);
        $http({
            method: 'POST',
            url: '/scripts/socketajax.php',
            data: data,  // pass in data as strings
            headers: {'Content-Type': 'application/x-www-form-urlencoded'} // set the headers so angular passing info as form data (not request payload)
        }).then(function(response) {
            var data = response.data;
            console.log(data.answer);
            if (data.answer == undefined || data.answer == false) {
                empty = "Ошибка сокета";
            } else {
                empty = empty + data.answer + "\r\n";
            }
            $scope.console = empty;

        }, function(error) {

            $scope.console.push("Error");

        });

    }


    controller.onPlayerReady = function(API) {
        controller.API = API;
    };

    controller.onCompleteVideo = function() {
        controller.isCompleted = true;
        controller.setVideo(0);
    };



    controller.config = {
        preload: "none",
        autoHide: false,
        autoHideTime: 3000,
        autoPlay: false,
        sources: [],//controller.videos[0].sources,
        theme: {
            url: "http://www.videogular.com/styles/themes/default/latest/videogular.css"
        },
        plugins: {
            poster: "http://www.videogular.com/assets/images/videogular.png"
        }
    };

    controller.setVideo = function(index) {
        controller.API.stop();
        controller.currentVideo = index;
        controller.config.sources = $scope.sources ;
        $timeout(controller.API.play.bind(controller.API), 100);
    };

    this.config = {
        tracks: [
            {
                src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
                kind: "subtitles",
                srclang: "en",
                label: "English",
                default: ""
            }
        ],
        theme: "bower_components/videogular-themes-default/videogular.css",
        plugins: {
            poster: "http://www.videogular.com/assets/images/videogular.png"
        }
    };


}]);


