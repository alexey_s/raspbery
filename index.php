<?php require_once("scripts/auth.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=960"/>
    <title></title>
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
<!--    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">-->
<!--    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js">-->
    <link rel="stylesheet" href="bower_components/nya-bootstrap-select/dist/css/nya-bs-select.min.css">
    <link rel="stylesheet" href="css/main.css">

    <link rel="stylesheet" href="bower_components/components-font-awesome/css/font-awesome.min.css">
</head>
<body>
<?php


if ($auth->isAuth()) { // Если пользователь авторизован, приветствуем:
    require_once("scripts/settings.php");
    require_once("scripts/panel.php");
}
else { //Если не авторизован, показываем форму ввода логина и пароля
    ?>
    <div class="container">
        <div class="row">
            <div class="block__auth">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <h3 class="panel-title">Пожалуйста авторизуйтесь!</h3>
                    </div>

                    <div class="panel-body">
                        <form accept-charset="UTF-8" role="form" method="post" action="">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Login" name="login" value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; // Заполняем поле по умолчанию ?>" type="text">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <input class="btn btn-lg btn-success btn-block" type="submit" value="Сохранить">
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
            
        </div>
    </div>
<?php } ?>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/angular/angular.min.js"></script>
<script src="bower_components/nya-bootstrap-select/dist/js/nya-bs-select.min.js"></script>
<script src="bower_components/angular-sanitize/angular-sanitize.min.js"></script>
<script src="bower_components/videogular/videogular.js"></script>
<script src="bower_components/videogular-controls/vg-controls.js"></script>
<script src="bower_components/videogular-overlay-play/vg-overlay-play.js"></script>
<script src="bower_components/videogular-poster/vg-poster.js"></script>
<script src="bower_components/videogular-buffering/vg-buffering.js"></script>
<script src="bower_components/angular/oi.file.js"></script>
<script src="http://tamtakoe.ru/uploader/lib/angular_1_2/angular-resource.min.js"></script>
        <script src="js/main.js"></script>
</body>

</html>